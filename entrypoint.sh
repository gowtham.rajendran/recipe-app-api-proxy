#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Ensure that nginx process runs as a foreground process on docker.
nginx -g "daemon off;"