FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="Gowtham Rajendran"

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_param /etc/nginx/uwsgi_param

RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static

COPY ./entrypoint.sh /entrypoint.sh

USER nginx
CMD ["/entrypoint.sh"]
